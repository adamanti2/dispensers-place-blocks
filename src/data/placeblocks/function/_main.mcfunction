from adamanti_bolt:utils import vanillaTag
from ./placeblocks import directions


function vanillaTag('load', ./load):
    scoreboard objectives add dpb_var dummy
    forceload add 0 0
    # schedule function ./check_items 1t replace
    schedule function ./spawn_helpers 10t replace

function ./spawn_helpers:
    schedule function ./spawn_helpers 7s replace
    store result score .count dpb_var if entity @e[type=armor_stand,tag=dpb_helper]
    if score .count dpb_var matches 2.. run kill @e[type=armor_stand,tag=dpb_helper]
    unless score .count dpb_var matches 1 run summon armor_stand 0 -64 0 {Small:1b,Marker:1b,Invisible:1b,Invulnerable:1b,NoGravity:1b,ShowArms:1b,Tags:[dpb_helper],DisabledSlots:4144959}


block_tag ./potentially_placeable:
    values:
        - "air"
        - "water"

def placeWithDirection(direction):
    scoreboard players set .direction dpb_var directions[direction]
    if data entity @e[type=item,limit=1,sort=nearest] Thrower run return 1
    function ./place_item

function vanillaTag('tick', ./tick):
    # schedule function ./check_items 2t replace
    as @e[type=item,tag=!dpb_checked] at @s:
        tag @s add dpb_checked
        unless block ~ ~ ~ #./potentially_placeable run return 0

        if block ~1 ~ ~ dispenser[facing=west] rotated 90 0 run return run execute:
            placeWithDirection("west")
        if block ~-1 ~ ~ dispenser[facing=east] rotated -90 0 run return run execute:
            placeWithDirection("east")
        if block ~ ~1 ~ dispenser[facing=down] rotated -90 90 run return run execute:
            placeWithDirection("down")
        if block ~ ~-1 ~ dispenser[facing=up] rotated -90 -90 run return run execute:
            placeWithDirection("up")
        if block ~ ~ ~1 dispenser[facing=north] rotated 180 0 run return run execute:
            placeWithDirection("north")
        if block ~ ~ ~-1 dispenser[facing=south] rotated 0 0 run return run execute:
            placeWithDirection("south")
