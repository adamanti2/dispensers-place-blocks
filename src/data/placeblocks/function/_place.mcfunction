from ./placeblocks import directions, item_to_block, heldTagPredicate, heldPredicate, placeBlock
from math import ceil


SPLITS_PER_TREE_LEVEL = (4, 4, 4, 4)
itemList = tuple(item_to_block.keys())
itemCount = len(itemList)

item_tag ./blocks {
    "values": itemList
}


predicatesOnLevel = {}
def itemListPredicate(items, level):
    if level not in predicatesOnLevel:
        predicatesOnLevel[level] = 0
    else:
        predicatesOnLevel[level] += 1
    predicatename = f"placeblocks:l{level}_{predicatesOnLevel[level]}"
    predicate predicatename {
        "condition": "minecraft:entity_properties",
        "entity": "this",
        "predicate": {
            "equipment": {
                "mainhand": {
                    "items": items
                }
            }
        }
    }
    return predicatename

def divideItemGroup(items, n):
    lists = []
    divisionSize = ceil(len(items) / n)
    for i in range(n):
        newList = []
        for j in range(divisionSize * i, divisionSize * (i + 1)):
            if j >= len(items):
                break
            newList.append(items[j])
        if len(newList) > 0:
            lists.append(newList)
    return lists


def node(level, group):
    level += 1
    if level <= len(SPLITS_PER_TREE_LEVEL):
        groups = divideItemGroup(group, SPLITS_PER_TREE_LEVEL[level - 1])
        for subgroup in groups:
            if predicate itemListPredicate(subgroup, level):
                node(level, subgroup)
    else:
        for item in group:
            if predicate itemListPredicate([item], level):
                placeBlock(item)

def tree():
    node(0, itemList)


function ./place_item:
    data modify entity @e[type=armor_stand,tag=dpb_helper,limit=1] HandItems[0] set from entity @s Item
    function ./place_current
    unless score .result dpb_var matches ..-1 if block ~ ~ ~ air run tag @e[type=item,tag=!dpb_checked,distance=..1] add dpb_checked
    unless score .result dpb_var matches ..-1 run kill @s

function ./place_current:
    scoreboard players set .result dpb_var -1
    as @e[type=armor_stand,tag=dpb_helper] if predicate heldTagPredicate("blocks") store result score .result dpb_var:
        scoreboard players set .result dpb_var 1
        scoreboard players set .waterlogged dpb_var 0

        if predicate heldTagPredicate("invert_direction"):
            scoreboard players operation .prevdirection dpb_var = .direction dpb_var
            if score .prevdirection dpb_var matches directions["east"] run scoreboard players set .direction dpb_var directions["west"]
            if score .prevdirection dpb_var matches directions["west"] run scoreboard players set .direction dpb_var directions["east"]
            if score .prevdirection dpb_var matches directions["north"] run scoreboard players set .direction dpb_var directions["south"]
            if score .prevdirection dpb_var matches directions["south"] run scoreboard players set .direction dpb_var directions["north"]
            if score .prevdirection dpb_var matches directions["up"] run scoreboard players set .direction dpb_var directions["down"]
            if score .prevdirection dpb_var matches directions["down"] run scoreboard players set .direction dpb_var directions["up"]
        
        if block ~ ~ ~ water run scoreboard players set .waterlogged dpb_var 1
        if predicate heldTagPredicate("need_water") unless block ~ ~ ~ water run return -3

        if score .direction dpb_var matches 4.. if predicate heldTagPredicate("facing_horizontal_only") run scoreboard players set .direction dpb_var 0
        if predicate heldTagPredicate("needs_air_above") unless block ~ ~1 ~ air run return -4
        if predicate heldTagPredicate("need_surface") unless block ^ ^ ^1 #./opaque run return -5
        if predicate heldTagPredicate("need_surface_above") unless block ~ ~1 ~ #./opaque run return -6
        if predicate heldTagPredicate("need_farmland_below") unless block ~ ~-1 ~ farmland run return -7
        if predicate heldTagPredicate("need_ground_below") unless block ~ ~-1 ~ #./opaque unless block ~ ~-1 ~ #slabs[type=double] unless block ~ ~-1 ~ #slabs[type=top] unless block ~ ~-1 ~ #stairs[half=top] run return -8
        if predicate heldPredicate("lily_pad") unless block ~ ~-1 ~ water run return -9
        if predicate heldPredicate("nether_wart") unless block ~ ~-1 ~ soul_sand run return -10

        tree()
        if score .result dpb_var matches ..-1 run return -1


# function ./test:
#     tree()
