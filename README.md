# Dispensers Place Blocks

A data pack for 1.21 that allows dispensers to place blocks in front of them when dispensed.
## Features
- When a dispenser dispenses a block, if possible, it will be placed.
- Blocks will be rotated the same direction as the dispenser is.
- Blocks placed in water will be waterlogged.
- Optimized for performance on servers, such that placing dispensed items is not a lag-intensive operation.
- Works with Adamanti's [Dispenser Minecarts](https://modrinth.com/datapack/dispenser-minecarts) data pack.

## Limitations
- A player standing too close to a dispenser will instantly pick up a block before the data pack is able to place it.
- Bugs related to blocks placed on incorrect surfaces may appear, although there are checks included to prevent most of them.
- May possibly work on future versions, but newly added blocks won't be placed.

## Technical details
- If a block has a `facing` block state, it will be set to the dispenser's `facing` state.
- Blocks with a `face` block state, as well as the `shape` block state of rails, will also be rotated.
- Checks every item entity only on the tick it is created, meaning low passive performance impact.
- Uses a search tree to match item IDs to block IDs which uses from 40 to 60 commands per block placed, meaning low performance impact on dispenser usage.
- This data pack was developed using [beet + bolt](https://github.com/mcbeet/bolt).

